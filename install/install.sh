#!/bin/bash

# step by step instructions available here: https://sdk.operatorframework.io/docs/install-operator-sdk/

RELEASE_VERSION=v0.19.0
BASE_URL=https://github.com/operator-framework/operator-sdk/releases/download/${RELEASE_VERSION}
GPG_KEY_ID=8018D6F1B58E194625E38581D16086E39AF46519

declare -A targets
targets[operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu]=operator-sdk 
targets[ansible-operator-${RELEASE_VERSION}-x86_64-linux-gnu]=ansible-operator
targets[helm-operator-${RELEASE_VERSION}-x86_64-linux-gnu]=helm-operator

sudo mkdir -p /usr/local/bin/
mkdir -p $RELEASE_VERSION && cd $RELEASE_VERSION
gpg --keyserver keyserver.ubuntu.com --recv-key $GPG_KEY_ID

function download_file() {
    file=$1
    if [[ ! -f $file ]]; then
      curl -LO $BASE_URL/$file
    fi

    if [[ ! -f $file.asc ]]; then 
      curl -LO $BASE_URL/$file.asc
    fi 

    if [[ -f $file ]] && [[ -f $file.asc ]]; then
      gpg --verify $file.asc
      chmod +x $file
      sudo cp $file /usr/local/bin/${targets[$file]}
    fi
}

for file in "${!targets[@]}"; do 
  download_file $file
done
