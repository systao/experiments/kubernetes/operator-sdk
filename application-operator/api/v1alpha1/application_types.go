/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ApplicationSpec struct {
	Service  ServiceSpec `json:"service,omitempty"`
	Replicas int32       `json:"replicas,omitempty"`
	Message  string      `json:"message"`
	Version  string      `json:"version"`
}

type ServiceSpec struct {
	Type   corev1.ServiceType `json:"type"`
	Expose ExposeSpec         `json:"expose,omitempty"`
}

type ExposeSpec struct {
	Hostname     string `json:"hostname"`
	IngressClass string `json:"ingressClass"`
	Path         string `json:"path"`
	Port         int    `json:"port"`
}

type ApplicationStatus struct {
	ServiceStatus ServiceStatus `json:"serviceType,omitempty"`
	PodNames      []string      `json:"podNames"`
}

type ServiceStatus struct {
	Name    string        `json:"name"`
	Ingress IngressStatus `json:"ingress,omitempty"`
}

type IngressStatus struct {
	Hostname string `json:"hostname"`
	Path     string `json:"path"`
	Port     int    `json:"port"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

type Application struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ApplicationSpec   `json:"spec,omitempty"`
	Status ApplicationStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

type ApplicationList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Application `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Application{}, &ApplicationList{})
}
