/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"net/url"
	"time"

	ctrl "sigs.k8s.io/controller-runtime"

	applicationv1alpha1 "gitlab.com/systao/application-operator/api/v1alpha1"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	extensions "k8s.io/api/extensions/v1beta1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	intstr "k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

// ApplicationReconciler reconciles a Application object
type ApplicationReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

// +kubebuilder:rbac:groups=application.systao.com,resources=applications,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=application.systao.com,resources=applications/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=extensions,resources=ingresses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=pods,verbs=get;list;
// +kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete

var log = logf.Log.WithName("controller_application")

// Add creates a new Application Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ApplicationReconciler{Client: mgr.GetClient(), Scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("application-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource Application
	err = c.Watch(&source.Kind{Type: &applicationv1alpha1.Application{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	// TODO(user): Modify this to be the types you create that are owned by the primary resource
	// Watch for changes to secondary resource Pods and requeue the owner Application
	err = c.Watch(&source.Kind{Type: &appsv1.Deployment{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &applicationv1alpha1.Application{},
	})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &corev1.Service{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &applicationv1alpha1.Application{},
	})

	err = c.Watch(&source.Kind{Type: &extensions.Ingress{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &applicationv1alpha1.Application{},
	})

	if err != nil {
		return err
	}

	return nil
}

// Reconcile runs the reconciliation loop
func (r *ApplicationReconciler) Reconcile(request ctrl.Request) (ctrl.Result, error) {
	_ = context.Background()

	// TODO: WTH is context.TODO() ??

	reqLogger := log.WithValues("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Info("Reconciling Application.")

	application := &applicationv1alpha1.Application{}
	err := r.Get(context.TODO(), request.NamespacedName, application)

	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			reqLogger.Info("Application resource not found. Ignoring since object must be deleted.")
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		reqLogger.Error(err, "Failed to get Application.")
		return reconcile.Result{}, err
	}

	/// DEPLOYMENT

	// Check if the Deployment already exists, if not create a new one
	deployment := &appsv1.Deployment{}
	err = r.Client.Get(context.TODO(), types.NamespacedName{Name: application.Name, Namespace: application.Namespace}, deployment)
	if err != nil && errors.IsNotFound(err) {
		// Define a new Deployment
		dep := r.deploymentForApplication(application)
		reqLogger.Info("Creating a new Deployment.", "Deployment.Namespace", dep.Namespace, "Deployment.Name", dep.Name)
		err = r.Client.Create(context.TODO(), dep)
		if err != nil {
			reqLogger.Error(err, "Failed to create new Deployment.", "Deployment.Namespace", dep.Namespace, "Deployment.Name", dep.Name)
			return reconcile.Result{}, err
		}

		// Deployment created successfully - return and requeue
		// NOTE: that the requeue is made with the purpose to provide the deployment object for the next step to ensure the deployment size is the same as the spec.
		return reconcile.Result{Requeue: true}, nil
	} else if err != nil {
		reqLogger.Error(err, "Failed to get Deployment.")
		return reconcile.Result{}, err
	}

	// Ensure the deployment size is the same as the spec
	size := application.Spec.Replicas
	if *deployment.Spec.Replicas != size {
		deployment.Spec.Replicas = &size
		err = r.Client.Update(context.TODO(), deployment)
		if err != nil {
			reqLogger.Error(err, "Failed to update Deployment.", "Deployment.Namespace", deployment.Namespace, "Deployment.Name", deployment.Name)
			return reconcile.Result{}, err
		}
	}

	/// SERVICE

	// Check if the Service already exists, if not create a new one
	service := &corev1.Service{}
	err = r.Client.Get(context.TODO(), types.NamespacedName{Name: application.Name, Namespace: application.Namespace}, service)
	if err != nil && errors.IsNotFound(err) {
		// Define a new Service
		svc := r.serviceForApplication(application)
		reqLogger.Info("Creating a new Service.", "Service.Namespace", svc.Namespace, "Service.Name", svc.Name)
		err = r.Client.Create(context.TODO(), svc)
		if err != nil {
			reqLogger.Error(err, "Failed to create new Service.", "Service.Namespace", svc.Namespace, "Service.Name", svc.Name)
			return reconcile.Result{}, err
		}

		// Service created successfully - return and requeue
		// NOTE: that the requeue is made with the purpose to provide the service object for the next step to ensure the ingress status is the same as expose spec.
		return reconcile.Result{Requeue: true}, nil
	} else if err != nil {
		reqLogger.Error(err, "Failed to get Service.")
		return reconcile.Result{}, err
	}

	// Ensure the service type is the same as the spec
	serviceType := application.Spec.Service.Type
	serviceSpecType := service.Spec.Type
	if serviceSpecType != serviceType {
		service.Spec.Type = serviceType
		err = r.Client.Update(context.TODO(), service)
		if err != nil {
			reqLogger.Error(err, "Failed to update Service.", "Service.Namespace", service.Namespace, "Service.Name", service.Name)
			return reconcile.Result{}, err
		}
	}

	// Ensure the ingress is as specified
	ingress := &extensions.Ingress{}
	expose := application.Spec.Service.Expose
	err = r.Client.Get(context.TODO(), types.NamespacedName{Name: application.Name, Namespace: application.Namespace}, ingress)
	if err != nil && errors.IsNotFound(err) {
		if serviceSpecType == corev1.ServiceTypeClusterIP && &expose != nil {
			ing := r.ingressForApplication(application)
			reqLogger.Info("Creating a new Ingress.", "Ingress.Namespace", ing.Namespace, "Ingress.Name", ing.Name)
			err = r.Client.Create(context.TODO(), ing)
			if err != nil {
				reqLogger.Error(err, "Failed to create new Ingress.", "Ingress.Namespace", ing.Namespace, "Ingress.Name", ing.Name)
				return reconcile.Result{}, err
			}

			// Ingress created successfully - return and requeue
			return reconcile.Result{Requeue: true}, nil
		}
	} else {
		if err == nil {
			if application.Spec.Service.Type != corev1.ServiceTypeClusterIP {
				reqLogger.Info("Deleting Ingress.", "Ingress.Namespace", ingress.Namespace, "Ingress.Name", ingress.Name)
				err = r.Client.Delete(context.TODO(), ingress)
				if err != nil {
					reqLogger.Error(err, "Failed to delete Ingress.", "Ingress.Namespace", ingress.Namespace, "Ingress.Name", ingress.Name)
					return reconcile.Result{}, err
				}

				// Ingress created successfully - return and requeue
				return reconcile.Result{Requeue: true}, nil
			} else if err != nil {
				reqLogger.Error(err, "Failed to get Ingress.")
				return reconcile.Result{}, err
			}
			// Ensure the ingress conforms to the spec
			ingressSpecPath := application.Spec.Service.Expose.Path
			ingressSpecPort := application.Spec.Service.Expose.Port
			ingressSpecHost := application.Spec.Service.Expose.Hostname
			ingressSpecClass := application.Spec.Service.Expose.IngressClass

			ingressRule := ingress.Spec.Rules[0]
			ingressHost := ingressRule.Host
			httpIngressRuleValue := ingressRule.IngressRuleValue.HTTP

			ingressPath := httpIngressRuleValue.Paths[0]

			reconcileIngressClass := &ingressSpecClass != nil && &ingressSpecClass != ingress.Spec.IngressClassName

			reqLogger.Info("Ingress class", "ingress.Spec.IngressClassName", *ingress.Spec.IngressClassName, "application.Spec.Service.Expose.IngressClass", ingressSpecClass)

			if ingressSpecHost != ingressHost ||
				reconcileIngressClass ||
				ingressSpecPath != ingressPath.Path ||
				intstr.FromInt(ingressSpecPort) != ingressPath.Backend.ServicePort ||
				application.Name != ingressPath.Backend.ServiceName {

				reqLogger.Info("Updating ingress",
					"ingressRule.Host", ingressSpecHost,
					"ingressPath.Backend.ServicePort", intstr.FromInt(ingressSpecPort),
					"ingressPath.Backend.ServiceName", application.Name,
					"ingressPath.Path", ingressSpecPath)

				ingress.Spec.IngressClassName = &ingressSpecClass
				ingress.Spec.Rules[0].Host = ingressSpecHost
				ingress.Spec.Rules[0].IngressRuleValue.HTTP.Paths[0].Backend.ServicePort = intstr.FromInt(ingressSpecPort)
				ingress.Spec.Rules[0].IngressRuleValue.HTTP.Paths[0].Backend.ServiceName = application.Name
				ingress.Spec.Rules[0].IngressRuleValue.HTTP.Paths[0].Path = ingressSpecPath

				err = r.Client.Update(context.TODO(), ingress)
				if err != nil {
					reqLogger.Error(err, "Failed to update Ingress.", "Ingress.Namespace", ingress.Namespace, "Ingress.Name", ingress.Name)
					return reconcile.Result{}, err
				}

			}
		}

	}

	// Update the Application status with the pod names
	// List the pods for this application's deployment
	podList := &corev1.PodList{}
	listOpts := []client.ListOption{
		client.InNamespace(application.Namespace),
		client.MatchingLabels(labelsForApplication(application.Name)),
	}
	err = r.Client.List(context.TODO(), podList, listOpts...)
	if err != nil {
		reqLogger.Error(err, "Failed to list pods.", "Application.Namespace", application.Namespace, "Application.Name", application.Name)
		return reconcile.Result{}, err
	}

	podNames := getPodNames(podList.Items)
	application.Status.PodNames = podNames

	return ctrl.Result{RequeueAfter: time.Second * 5}, nil
}

// SetupWithManager registers the controller with the manager
func (r *ApplicationReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&applicationv1alpha1.Application{}).
		Owns(&appsv1.Deployment{}).
		Complete(r)
}

// deploymentForApplication returns an application Deployment object
func (r *ApplicationReconciler) deploymentForApplication(application *applicationv1alpha1.Application) *appsv1.Deployment {
	ls := labelsForApplication(application.Name)
	replicas := application.Spec.Replicas
	version := application.Spec.Version
	message := application.Spec.Message

	params := url.Values{}
	params.Add("message", message)
	params.Add("format", "html")

	query := "http://cowsay.morecode.org/say?" + params.Encode()

	dep := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      application.Name,
			Namespace: application.Namespace,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: &replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: ls,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: ls,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{{
						Image: "nginx:" + version,
						Name:  "application",
						VolumeMounts: []corev1.VolumeMount{{
							MountPath: "/usr/share/nginx/html",
							Name:      "scratch",
							ReadOnly:  true,
						}},
					}, {
						Image: "busybox",
						Name:  "cowsay",
						Command: []string{
							"/bin/sh",
							"-c",
							"wget -O /cowsay/index.html '" + query + "' && sleep 3600",
						},
						VolumeMounts: []corev1.VolumeMount{{
							MountPath: "/cowsay",
							Name:      "scratch",
						}},
					}},
					Volumes: []corev1.Volume{{
						Name: "scratch",
						VolumeSource: corev1.VolumeSource{
							EmptyDir: &corev1.EmptyDirVolumeSource{},
						},
					}},
				},
			},
		},
	}
	// Set Applicaiton instance as the owner of the Deployment.
	controllerutil.SetControllerReference(application, dep, r.Scheme)
	return dep
}

// serviceForApplication function takes in an Application object and returns a Service for that object.
func (r *ApplicationReconciler) serviceForApplication(application *applicationv1alpha1.Application) *corev1.Service {
	ls := labelsForApplication(application.Name)

	svc := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      application.Name,
			Namespace: application.Namespace,
			Labels:    ls,
		},
		Spec: corev1.ServiceSpec{
			Selector: ls,
			Type:     application.Spec.Service.Type,
			Ports: []corev1.ServicePort{
				{
					Port: 80,
					Name: "http",
				},
			},
		},
	}
	// Set Application instance as the owner of the Service.
	controllerutil.SetControllerReference(application, svc, r.Scheme)
	return svc
}

// ingressForApplication function takes in an Application object and returns an Ingress for that object.
func (r *ApplicationReconciler) ingressForApplication(application *applicationv1alpha1.Application) *extensions.Ingress {
	ls := labelsForApplication(application.Name)

	ing := &extensions.Ingress{
		ObjectMeta: metav1.ObjectMeta{
			Name:      application.Name,
			Namespace: application.Namespace,
			Labels:    ls,
		},
		Spec: extensions.IngressSpec{
			Rules: []extensions.IngressRule{{
				Host: application.Spec.Service.Expose.Hostname,
				IngressRuleValue: extensions.IngressRuleValue{
					HTTP: &extensions.HTTPIngressRuleValue{
						Paths: []extensions.HTTPIngressPath{{
							Path: application.Spec.Service.Expose.Path,
							Backend: extensions.IngressBackend{
								ServiceName: application.Name,
								ServicePort: intstr.FromInt(application.Spec.Service.Expose.Port),
							},
						}},
					},
				},
			}},
		},
	}

	ingressClass := application.Spec.Service.Expose.IngressClass
	if &ingressClass != nil {
		ing.Spec.IngressClassName = &ingressClass
	}

	// Set Application instance as the owner of the Service.
	controllerutil.SetControllerReference(application, ing, r.Scheme)
	return ing
}

// labelsForApplication returns the labels for selecting the resources
// belonging to the given application CR name.
func labelsForApplication(name string) map[string]string {
	return map[string]string{"app": "application", "application": name}
}

// getPodNames returns the pod names of the array of pods passed in
func getPodNames(pods []corev1.Pod) []string {
	var podNames []string
	for _, pod := range pods {
		podNames = append(podNames, pod.Name)
	}
	return podNames
}
